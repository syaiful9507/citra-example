﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Modul12
{
    public partial class Form1 : Form
    {
        Bitmap objBitmap;
        Bitmap objBitmap1;
        Bitmap objBitmap2;
        public Form1()
        {
            InitializeComponent();
        }
        private void btn_load_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                objBitmap = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Image = objBitmap;
            }
        }

        private void btn_histogram_Click(object sender, EventArgs e)
        {
            float[] h = new float[384];
            int i;
            for (i = 0; i < 384; i++) h[i] = 0;
            for (int x = 0; x < objBitmap.Width; x++)
                for (int y = 0; y < objBitmap.Height; y++)
                {
                    Color w = objBitmap.GetPixel(x, y);
                    int r = w.R/2;
                    int g = w.G/2;
                    int b = w.B/2;
                    h[r] = h[r] ++;
                    h[g] = h[128+g] ++;
                    h[b] = h[256+b] ++;
                }
            for (i = 0; i < 384; i++)
            {
                chart1.Series["Series1"].Points.AddXY(i, h[i]);
            }
        }

        private void btn_load1_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                objBitmap1 = new Bitmap(openFileDialog1.FileName);
                pictureBox2.Image = objBitmap1;
            }
        }

        private void btn_histogram1_Click(object sender, EventArgs e)
        {
            float[] h = new float[384];
            int i;
            for (i = 0; i < 384; i++) h[i] = 0;
            for (int x = 0; x < objBitmap1.Width; x++)
                for (int y = 0; y < objBitmap1.Height; y++)
                {
                    Color w = objBitmap1.GetPixel(x, y);
                    int r = w.R/2;
                    int g = w.G/2;
                    int b = w.B/2;
                    h[r] = h[r] ++;
                    h[g] = h[128+g] ++;
                    h[b] = h[256+b] ++;
                }
            for (i = 0; i < 384; i++)
            {
                chart2.Series["Series1"].Points.AddXY(i, h[i]);
            }
        }

        private void btn_load2_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                objBitmap2 = new Bitmap(openFileDialog1.FileName);
                pictureBox3.Image = objBitmap2;
            }
        }

        private void btn_histogram2_Click(object sender, EventArgs e)
        {
            float[] h = new float[384];
            int i;
            for (i = 0; i < 384; i++) h[i] = 0;
            for (int x = 0; x < objBitmap2.Width; x++)
                for (int y = 0; y < objBitmap2.Height; y++)
                {
                    Color w = objBitmap2.GetPixel(x, y);
                    int r = w.R/2;
                    int g = w.G/2;
                    int b = w.B/2;
                    h[r] = h[r] ++;
                    h[g] = h[128+g] ++;
                    h[b] = h[256+b] ++;
                }
            for (i = 0; i < 384; i++)
            {
                chart3.Series["Series1"].Points.AddXY(i, h[i]);
            }
        }

        private void btn_match_Click(object sender, EventArgs e)
        {
            float[] d = new float[3];
            float di = 0;
            int j, i;
            for (i = 0; i < 3; i++)
            {
                d[i] = 0;
                for (j = 0; j < 384; j++)
                {
                    di = di + (float)Math.Abs(h[3, j - h[i, j]]);
                }
                di = di / 384;
                d[i] = di;
            }
            label4.Text = d[0].ToString();
            label5.Text = d[1].ToString();
            label6.Text = d[2].ToString();
            if ((d[0] < d[1]) && (d[0] < d[2]))
            {
                label7.Text = "Hijau";
            }
            if ((d[1] < d[0]) && (d[1] < d[2]))
            {
                label7.Text = "Merah";
            }
            if ((d[2] < d[0]) && (d[2] < d[1]))
            {
                label7.Text = "Campuran";
            }

        }
    }
}
