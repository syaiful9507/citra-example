﻿namespace Modul12
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.btn_load = new System.Windows.Forms.Button();
            this.btn_histogram = new System.Windows.Forms.Button();
            this.btn_load1 = new System.Windows.Forms.Button();
            this.btn_histogram1 = new System.Windows.Forms.Button();
            this.btn_load2 = new System.Windows.Forms.Button();
            this.btn_histogram2 = new System.Windows.Forms.Button();
            this.btn_interseksi = new System.Windows.Forms.Button();
            this.btn_load3 = new System.Windows.Forms.Button();
            this.btn_histogram3 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart5 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btn_match = new System.Windows.Forms.Button();
            this.btn_histoo1 = new System.Windows.Forms.Button();
            this.btn_histoo2 = new System.Windows.Forms.Button();
            this.btn_histoo3 = new System.Windows.Forms.Button();
            this.btn_histoo4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_load
            // 
            this.btn_load.Location = new System.Drawing.Point(132, 12);
            this.btn_load.Name = "btn_load";
            this.btn_load.Size = new System.Drawing.Size(51, 102);
            this.btn_load.TabIndex = 0;
            this.btn_load.Text = "LOAD";
            this.btn_load.UseVisualStyleBackColor = true;
            this.btn_load.Click += new System.EventHandler(this.btn_load_Click);
            // 
            // btn_histogram
            // 
            this.btn_histogram.Location = new System.Drawing.Point(64, 12);
            this.btn_histogram.Name = "btn_histogram";
            this.btn_histogram.Size = new System.Drawing.Size(62, 102);
            this.btn_histogram.TabIndex = 1;
            this.btn_histogram.Text = "Histogram";
            this.btn_histogram.UseVisualStyleBackColor = true;
            this.btn_histogram.Click += new System.EventHandler(this.btn_histogram_Click);
            // 
            // btn_load1
            // 
            this.btn_load1.Location = new System.Drawing.Point(132, 120);
            this.btn_load1.Name = "btn_load1";
            this.btn_load1.Size = new System.Drawing.Size(51, 94);
            this.btn_load1.TabIndex = 2;
            this.btn_load1.Text = "LOAD";
            this.btn_load1.UseVisualStyleBackColor = true;
            this.btn_load1.Click += new System.EventHandler(this.btn_load1_Click);
            // 
            // btn_histogram1
            // 
            this.btn_histogram1.Location = new System.Drawing.Point(64, 120);
            this.btn_histogram1.Name = "btn_histogram1";
            this.btn_histogram1.Size = new System.Drawing.Size(62, 94);
            this.btn_histogram1.TabIndex = 3;
            this.btn_histogram1.Text = "Histogram";
            this.btn_histogram1.UseVisualStyleBackColor = true;
            this.btn_histogram1.Click += new System.EventHandler(this.btn_histogram1_Click);
            // 
            // btn_load2
            // 
            this.btn_load2.Location = new System.Drawing.Point(132, 220);
            this.btn_load2.Name = "btn_load2";
            this.btn_load2.Size = new System.Drawing.Size(51, 92);
            this.btn_load2.TabIndex = 4;
            this.btn_load2.Text = "LOAD";
            this.btn_load2.UseVisualStyleBackColor = true;
            this.btn_load2.Click += new System.EventHandler(this.btn_load2_Click);
            // 
            // btn_histogram2
            // 
            this.btn_histogram2.Location = new System.Drawing.Point(64, 220);
            this.btn_histogram2.Name = "btn_histogram2";
            this.btn_histogram2.Size = new System.Drawing.Size(62, 92);
            this.btn_histogram2.TabIndex = 5;
            this.btn_histogram2.Text = "Histogram";
            this.btn_histogram2.UseVisualStyleBackColor = true;
            this.btn_histogram2.Click += new System.EventHandler(this.btn_histogram2_Click);
            // 
            // btn_interseksi
            // 
            this.btn_interseksi.Location = new System.Drawing.Point(64, 318);
            this.btn_interseksi.Name = "btn_interseksi";
            this.btn_interseksi.Size = new System.Drawing.Size(119, 112);
            this.btn_interseksi.TabIndex = 6;
            this.btn_interseksi.Text = "Interseksi";
            this.btn_interseksi.UseVisualStyleBackColor = true;
            // 
            // btn_load3
            // 
            this.btn_load3.Location = new System.Drawing.Point(132, 436);
            this.btn_load3.Name = "btn_load3";
            this.btn_load3.Size = new System.Drawing.Size(51, 101);
            this.btn_load3.TabIndex = 7;
            this.btn_load3.Text = "LOAD";
            this.btn_load3.UseVisualStyleBackColor = true;
            // 
            // btn_histogram3
            // 
            this.btn_histogram3.Location = new System.Drawing.Point(64, 436);
            this.btn_histogram3.Name = "btn_histogram3";
            this.btn_histogram3.Size = new System.Drawing.Size(62, 101);
            this.btn_histogram3.TabIndex = 8;
            this.btn_histogram3.Text = "Histogram";
            this.btn_histogram3.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(189, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(122, 102);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(189, 120);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(122, 94);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(189, 220);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(122, 92);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(189, 436);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(122, 101);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            // 
            // chart1
            // 
            chartArea6.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chart1.Legends.Add(legend6);
            this.chart1.Location = new System.Drawing.Point(326, 12);
            this.chart1.Name = "chart1";
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.chart1.Series.Add(series6);
            this.chart1.Size = new System.Drawing.Size(477, 102);
            this.chart1.TabIndex = 13;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea7.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea7);
            legend7.Name = "Legend1";
            this.chart2.Legends.Add(legend7);
            this.chart2.Location = new System.Drawing.Point(326, 120);
            this.chart2.Name = "chart2";
            series7.ChartArea = "ChartArea1";
            series7.Legend = "Legend1";
            series7.Name = "Series1";
            this.chart2.Series.Add(series7);
            this.chart2.Size = new System.Drawing.Size(477, 94);
            this.chart2.TabIndex = 14;
            this.chart2.Text = "chart2";
            // 
            // chart3
            // 
            chartArea8.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea8);
            legend8.Name = "Legend1";
            this.chart3.Legends.Add(legend8);
            this.chart3.Location = new System.Drawing.Point(326, 220);
            this.chart3.Name = "chart3";
            series8.ChartArea = "ChartArea1";
            series8.Legend = "Legend1";
            series8.Name = "Series1";
            this.chart3.Series.Add(series8);
            this.chart3.Size = new System.Drawing.Size(477, 92);
            this.chart3.TabIndex = 15;
            this.chart3.Text = "chart3";
            // 
            // chart4
            // 
            chartArea9.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea9);
            legend9.Name = "Legend1";
            this.chart4.Legends.Add(legend9);
            this.chart4.Location = new System.Drawing.Point(326, 318);
            this.chart4.Name = "chart4";
            series9.ChartArea = "ChartArea1";
            series9.Legend = "Legend1";
            series9.Name = "Series1";
            this.chart4.Series.Add(series9);
            this.chart4.Size = new System.Drawing.Size(477, 112);
            this.chart4.TabIndex = 16;
            this.chart4.Text = "chart4";
            // 
            // chart5
            // 
            chartArea10.Name = "ChartArea1";
            this.chart5.ChartAreas.Add(chartArea10);
            legend10.Name = "Legend1";
            this.chart5.Legends.Add(legend10);
            this.chart5.Location = new System.Drawing.Point(326, 436);
            this.chart5.Name = "chart5";
            series10.ChartArea = "ChartArea1";
            series10.Legend = "Legend1";
            series10.Name = "Series1";
            this.chart5.Series.Add(series10);
            this.chart5.Size = new System.Drawing.Size(477, 121);
            this.chart5.TabIndex = 17;
            this.chart5.Text = "chart5";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btn_match
            // 
            this.btn_match.Location = new System.Drawing.Point(189, 318);
            this.btn_match.Name = "btn_match";
            this.btn_match.Size = new System.Drawing.Size(122, 23);
            this.btn_match.TabIndex = 18;
            this.btn_match.Text = "Matching ";
            this.btn_match.UseVisualStyleBackColor = true;
            this.btn_match.Click += new System.EventHandler(this.btn_match_Click);
            // 
            // btn_histoo1
            // 
            this.btn_histoo1.Location = new System.Drawing.Point(809, 12);
            this.btn_histoo1.Name = "btn_histoo1";
            this.btn_histoo1.Size = new System.Drawing.Size(68, 102);
            this.btn_histoo1.TabIndex = 19;
            this.btn_histoo1.Text = "Histogram";
            this.btn_histoo1.UseVisualStyleBackColor = true;
            // 
            // btn_histoo2
            // 
            this.btn_histoo2.Location = new System.Drawing.Point(809, 120);
            this.btn_histoo2.Name = "btn_histoo2";
            this.btn_histoo2.Size = new System.Drawing.Size(68, 94);
            this.btn_histoo2.TabIndex = 20;
            this.btn_histoo2.Text = "Hitstogram";
            this.btn_histoo2.UseVisualStyleBackColor = true;
            // 
            // btn_histoo3
            // 
            this.btn_histoo3.Location = new System.Drawing.Point(809, 220);
            this.btn_histoo3.Name = "btn_histoo3";
            this.btn_histoo3.Size = new System.Drawing.Size(68, 92);
            this.btn_histoo3.TabIndex = 21;
            this.btn_histoo3.Text = "Histogram";
            this.btn_histoo3.UseVisualStyleBackColor = true;
            // 
            // btn_histoo4
            // 
            this.btn_histoo4.Location = new System.Drawing.Point(809, 436);
            this.btn_histoo4.Name = "btn_histoo4";
            this.btn_histoo4.Size = new System.Drawing.Size(68, 101);
            this.btn_histoo4.TabIndex = 22;
            this.btn_histoo4.Text = "Histogram";
            this.btn_histoo4.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(189, 344);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(189, 362);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(189, 379);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(226, 344);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(226, 362);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "label5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(226, 379);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "label6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(235, 417);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "label7";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 572);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_histoo4);
            this.Controls.Add(this.btn_histoo3);
            this.Controls.Add(this.btn_histoo2);
            this.Controls.Add(this.btn_histoo1);
            this.Controls.Add(this.btn_match);
            this.Controls.Add(this.chart5);
            this.Controls.Add(this.chart4);
            this.Controls.Add(this.chart3);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_histogram3);
            this.Controls.Add(this.btn_load3);
            this.Controls.Add(this.btn_interseksi);
            this.Controls.Add(this.btn_histogram2);
            this.Controls.Add(this.btn_load2);
            this.Controls.Add(this.btn_histogram1);
            this.Controls.Add(this.btn_load1);
            this.Controls.Add(this.btn_histogram);
            this.Controls.Add(this.btn_load);
            this.Name = "Form1";
            this.Text = "Andreas Nugroho";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_load;
        private System.Windows.Forms.Button btn_histogram;
        private System.Windows.Forms.Button btn_load1;
        private System.Windows.Forms.Button btn_histogram1;
        private System.Windows.Forms.Button btn_load2;
        private System.Windows.Forms.Button btn_histogram2;
        private System.Windows.Forms.Button btn_interseksi;
        private System.Windows.Forms.Button btn_load3;
        private System.Windows.Forms.Button btn_histogram3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn_match;
        private System.Windows.Forms.Button btn_histoo1;
        private System.Windows.Forms.Button btn_histoo2;
        private System.Windows.Forms.Button btn_histoo3;
        private System.Windows.Forms.Button btn_histoo4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

