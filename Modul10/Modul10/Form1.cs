﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Modul10
{
    public partial class Form1 : Form
    {

        Bitmap objBitmap;
        Bitmap objBitmap1;
        Bitmap objBitmap2;
        Bitmap objBitmap3;
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_load_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                objBitmap = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Image = objBitmap;
            }
        }

        private void btn_hr_Click(object sender, EventArgs e)
        {
            objBitmap1 = new Bitmap(objBitmap);
            float[] h = new float[256];
            int i;
            for (i = 0; i < 256; i++) h[i] = 0;
            for (int x = 0; x < objBitmap1.Width; x++)
                for (int y = 0; y < objBitmap1.Height; y++)
                {
                    Color w = objBitmap1.GetPixel(x, y);
                    int xg = w.R;
                    h[xg] = h[xg] + 1;
                }
            for (i = 0; i < 256; i++)
            {
                chart1.Series["Series1"].Points.AddXY(i, h[i]);
            }
        }

        private void btn_hg_Click(object sender, EventArgs e)
        {
            objBitmap2 = new Bitmap(objBitmap);
            float[] h = new float[256];
            int i;
            for (i = 0; i < 256; i++) h[i] = 0;
            for (int x = 0; x < objBitmap2.Width; x++)
                for (int y = 0; y < objBitmap2.Height; y++)
                {
                    Color w = objBitmap2.GetPixel(x, y);
                    int xg = w.G;
                    h[xg] = h[xg] + 1;
                }
            for (i = 0; i < 256; i++)
            {
                chart2.Series["Series1"].Points.AddXY(i, h[i]);
            }
        }

        private void btn_hb_Click(object sender, EventArgs e)
        {
            objBitmap3 = new Bitmap(objBitmap);
            float[] h = new float[256];
            int i;
            for (i = 0; i < 256; i++) h[i] = 0;
            for (int x = 0; x < objBitmap3.Width; x++)
                for (int y = 0; y < objBitmap3.Height; y++)
                {
                    Color w = objBitmap3.GetPixel(x, y);
                    int xg = w.B;
                    h[xg] = h[xg] + 1;
                }
            for (i = 0; i < 256; i++)
            {
                chart3.Series["Series1"].Points.AddXY(i, h[i]);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
