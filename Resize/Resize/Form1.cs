﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Resize
{
    public partial class Form1 : Form
    {

        Image File;
        Bitmap objBitmap;
        Bitmap objBitmap1; 
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_load_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                objBitmap = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Image = objBitmap;
            } 
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            int nilai;
            objBitmap1 = new Bitmap(objBitmap);
            nilai = Convert.ToInt32(trackBar1.Value);
            int getWidth = objBitmap.Width;
            int getHeight = objBitmap.Height;
            double ratio = 0;
            if (getWidth > getHeight)
            {
                ratio = getWidth / nilai;
                getWidth = nilai;
                getHeight = (int)(getHeight / ratio);
            }
            else
            {
            ratio = getHeight / nilai;
            getHeight = nilai;
            getWidth = (int)(getWidth / ratio);
            }
            pictureBox2.Width = getWidth;
            pictureBox2.Height = getHeight;
            pictureBox2.Image = objBitmap1;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            File = pictureBox2.Image;
            DialogResult d = saveFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            { 
                File.Save(saveFileDialog1.FileName, ImageFormat.Jpeg); 
            } 
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
