﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace Praktikum_Modul_12_Zedgraph
{
    public partial class Form1 : Form
    {

        Bitmap img, img2, img3, img4;
        int[,] h = new int[4, 384];
        int[,] hs = new int[4, 384];
        int[] gn = new int[384];
        int i;
        int currentImgWidth;
        int currentImgHeight;
        GraphPane myPane = new GraphPane();
        GraphPane myPane2 = new GraphPane();
        GraphPane myPane3 = new GraphPane();
        GraphPane myPane4 = new GraphPane();
        GraphPane myPane5 = new GraphPane();

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_load1_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFile = new OpenFileDialog();
            if (oFile.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                img = new Bitmap(new Bitmap(oFile.FileName), pictureBox1.Width, pictureBox1.Height);
                pictureBox1.Image = img;
            }
            histoRed();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            myPane.CurveList.Clear();
            myPane = zedGraphControl1.GraphPane;
            myPane.CurveList.Clear();
            myPane2 = zedGraphControl2.GraphPane;
            myPane3.CurveList.Clear();
            myPane3 = zedGraphControl3.GraphPane;
            myPane4.CurveList.Clear();
            myPane4 = zedGraphControl4.GraphPane;
            myPane5.CurveList.Clear();
            myPane5 = zedGraphControl5.GraphPane;
       
        }

        private void histoRed()
        {
            myPane.CurveList.Clear();
            int r, g, b;
            PointPairList dataGraph = new PointPairList();
            for (i = 0; i < 384; i++)
                h[0, i] = 0;
            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    r = img.GetPixel(i, j).R;
                    g = img.GetPixel(i, j).G;
                    b = img.GetPixel(i, j).B;
                    r /= 2;
                    g /= 2;
                    b /= 2;
                    h[0, r]++;
                    h[0, 128 + g]++;
                    h[0, 256 + b]++;
                }
            }
            int hmax = h[0, 0];
            for (i = 1; i < 384; i++)
            {
                if (h[0, i] > hmax)
                    hmax = h[0, i];
            }
            for (i = 0; i < 384; i++)
                h[0, i] = 120 * h[0, i] / hmax;
            for (int i = 0; i < 384; i++)
            {
                dataGraph.Add(i, h[0, i]);
            }
            LineItem myCurve = myPane.AddCurve("data", dataGraph, Color.Black, SymbolType.None);
            myCurve.Line.Fill = new Fill(Color.Black, Color.Black, 45f);
            myPane.XAxis.Scale.Max = 386;
            zedGraphControl1.AxisChange();
            myPane.AxisChange();
            zedGraphControl1.Refresh();
        }

        private void btn_load2_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFile = new OpenFileDialog();
            if (oFile.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                img2 = new Bitmap(new Bitmap(oFile.FileName), pictureBox2.Width, pictureBox2.Height);
                pictureBox2.Image = img2;
            }
            myPane2.CurveList.Clear();
            histoGreen();
            myPane2.AxisChange();
            zedGraphControl2.Refresh();
        }

        private void histoGreen()
        {
            int r, g, b;
            PointPairList dataGraph = new PointPairList();
            for (i = 0; i < 384; i++)
                h[1, i] = 0;
            for (int i = 0; i < img2.Width; i++)
            {
                for (int j = 0; j < img2.Height; j++)
                {
                    r = img2.GetPixel(i, j).R;
                    g = img2.GetPixel(i, j).G;
                    b = img2.GetPixel(i, j).B;
                    r /= 2;
                    g /= 2;
                    b /= 2;
                    h[1, r]++;
                    h[1, 128 + g]++;
                    h[1, 256 + b]++;
                }
            }
            int hmax = h[1, 0];
            for (i = 1; i < 384; i++)
            {
                if (h[1, i] > hmax)
                    hmax = h[1, i];
            }
            for (i = 0; i < 384; i++)
                h[1, i] = 120 * h[1, i] / hmax;
            for (int i = 0; i < 384; i++)
            {
                dataGraph.Add(i, h[1, i]);
            }
            LineItem myCurve = myPane2.AddCurve("data", dataGraph, Color.Black, SymbolType.None);
            myCurve.Line.Fill = new Fill(Color.Black, Color.Black, 45f);
            myPane2.XAxis.Scale.Max = 386;
            zedGraphControl2.AxisChange();
        }

        private void btn_load3_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFile = new OpenFileDialog();
            if (oFile.ShowDialog() == DialogResult.OK)
            {
                pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                img3 = new Bitmap(new Bitmap(oFile.FileName), pictureBox3.Width, pictureBox3.Height);
                pictureBox3.Image = img3;
            }
            myPane3.CurveList.Clear();
            histoBlue();
            myPane3.AxisChange();
            zedGraphControl3.Refresh();
        }

        private void histoBlue()
        {
            int r, g, b;
            PointPairList dataGraph = new PointPairList();
            for (i = 0; i < 384; i++)
                h[2, i] = 0;
            for (int i = 0; i < img3.Width; i++)
            {
                for (int j = 0; j < img3.Height; j++)
                {
                    r = img3.GetPixel(i, j).R;
                    g = img3.GetPixel(i, j).G;
                    b = img3.GetPixel(i, j).B;
                    r /= 2;
                    g /= 2;
                    b /= 2;
                    h[2, r]++;
                    h[2, 128 + g]++;
                    h[2, 256 + b]++;
                }
            }
            int hmax = h[2, 0];
            for (i = 1; i < 384; i++)
            {
                if (h[2, i] > hmax)
                    hmax = h[2, i];
            }
            for (i = 0; i < 384; i++)
                h[2, i] = 120 * h[2, i] / hmax;
            for (int i = 0; i < 384; i++)
            {
                dataGraph.Add(i, h[2, i]);
            }
            LineItem myCurve = myPane3.AddCurve("data", dataGraph, Color.Black, SymbolType.None);
            myCurve.Line.Fill = new Fill(Color.Black, Color.Black, 45f);
            myPane3.XAxis.Scale.Max = 386;
            zedGraphControl3.AxisChange();
        }

        private void btn_interseksi_Click(object sender, EventArgs e)
        {
            inter();
            histoInter1();
            histoInter2();
            histoInter3();
            histoInter4();
        }

        private void inter()
        {
            myPane4.CurveList.Clear();
            PointPairList dataGraph = new PointPairList();
            for (i = 0; i < 384; i++)
            {
                gn[i] = h[0, i];
                if (h[1, i] < gn[i]) gn[i] = h[1, i];
                if (h[2, i] < gn[i]) gn[i] = h[2, i];
            }
            for (i = 0; i < 384; i++)
            {
                dataGraph.Add(i, gn[i]);
            }
            LineItem myCurve = myPane4.AddCurve("data", dataGraph, Color.Black, SymbolType.None);
            myCurve.Line.Fill = new Fill(Color.Black, Color.Black, 45f);
            myPane4.XAxis.Scale.Max = 386;
            zedGraphControl4.AxisChange();
            myPane4.AxisChange();
            zedGraphControl4.Refresh();
        }

        private void histoInter1()
        {
            for (i = 0; i < 384; i++)
                hs[0, i] = Math.Abs(h[0, i] - gn[i]);
            myPane.CurveList.Clear();
            zedGraphControl1.Refresh();
            PointPairList dataGraph = new PointPairList();
            for (int i = 0; i < 384; i++)
            {
                dataGraph.Add(i, hs[0, i]);
            }
            LineItem myCurve = myPane.AddCurve("data", dataGraph, Color.Red, SymbolType.None);
            myCurve.Line.Fill = new Fill(Color.Red, Color.Red, 45f);
            myPane.XAxis.Scale.Max = 386;
            zedGraphControl1.AxisChange();
            myPane.AxisChange();
            zedGraphControl1.Refresh();
        }
        private void histoInter2()
        {
            for (i = 0; i < 384; i++)
                hs[1, i] = Math.Abs(h[1, i] - gn[i]);
            myPane2.CurveList.Clear();
            zedGraphControl2.Refresh();
            PointPairList dataGraph = new PointPairList();
            for (int i = 0; i < 384; i++)
            {
                dataGraph.Add(i, hs[1, i]);
            }
            LineItem myCurve = myPane2.AddCurve("data", dataGraph, Color.Red, SymbolType.None);
            myCurve.Line.Fill = new Fill(Color.Red, Color.Red, 45f);
            myPane2.XAxis.Scale.Max = 386;
            zedGraphControl2.AxisChange();
            myPane2.AxisChange();
            zedGraphControl2.Refresh();
        }
        private void histoInter3()
        {
            for (i = 0; i < 384; i++)
                hs[2, i] = Math.Abs(h[2, i] - gn[i]);
            myPane3.CurveList.Clear();
            zedGraphControl3.Refresh();
            PointPairList dataGraph = new PointPairList();
            for (int i = 0; i < 384; i++)
            {
                dataGraph.Add(i, hs[2, i]);
            }
            LineItem myCurve = myPane3.AddCurve("data", dataGraph, Color.Red, SymbolType.None);
            myCurve.Line.Fill = new Fill(Color.Red, Color.Red, 45f);
            myPane3.XAxis.Scale.Max = 386;
            zedGraphControl3.AxisChange();
            myPane3.AxisChange();
            zedGraphControl3.Refresh();
        }
        private void histoInter4()
        {
            for (i = 0; i < 384; i++)
                hs[3, i] = Math.Abs(h[3, i] - gn[i]);
            myPane5.CurveList.Clear();
            zedGraphControl5.Refresh();
            PointPairList dataGraph = new PointPairList();
            for (int i = 0; i < 384; i++)
            {
                dataGraph.Add(i, hs[3, i]);
            }
            LineItem myCurve = myPane5.AddCurve("data", dataGraph, Color.Red, SymbolType.None);
            myCurve.Line.Fill = new Fill(Color.Red, Color.Red, 45f);
            myPane5.XAxis.Scale.Max = 386;
            zedGraphControl5.AxisChange();
            myPane5.AxisChange();
            zedGraphControl5.Refresh();
        }

        private void btn_match_Click(object sender, EventArgs e)
        {
            float[] d = new float[3];
            float di = 0;
            int j;
            for (i = 0; i < 3; i++)
            {
                d[i] = 0;
                for (j = 0; j < 384; j++)
                {
                    di = di + (float)Math.Abs(hs[3, j - hs[i, j]]);
                }
                di = di / 384;
                d[i] = di;
            }
            label4.Text = d[0].ToString();
            label5.Text = d[1].ToString();
            label6.Text = d[2].ToString();
            if ((d[0] < d[1]) && (d[0] < d[2]))
            {
                label7.Text = "Hijau";
            }
            if ((d[1] < d[0]) && (d[1] < d[2]))
            {
                label7.Text = "Merah";
            }
            if ((d[2] < d[0]) && (d[2] < d[1]))
            {
                label7.Text = "Campuran";
            }
        }

        private void btn_load4_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFile = new OpenFileDialog();
            if (oFile.ShowDialog() == DialogResult.OK)
            {
                pictureBox4.SizeMode = PictureBoxSizeMode.Zoom;
                img4 = new Bitmap(new Bitmap(oFile.FileName), pictureBox4.Width, pictureBox4.Height);
                pictureBox4.Image = img4;
            }
            histoGbr4();
        }

        private void histoGbr4()
        {
            myPane5.CurveList.Clear();
            int r, g, b;
            PointPairList dataGraph = new PointPairList();
            for (i = 0; i < 384; i++)
                h[3, i] = 0;
            for (int i = 0; i < img4.Width; i++)
            {
                for (int j = 0; j < img4.Height; j++)
                {
                    r = img4.GetPixel(i, j).R;
                    g = img4.GetPixel(i, j).G;
                    b = img4.GetPixel(i, j).B;
                    r /= 2;
                    g /= 2;
                    b /= 2;
                    h[3, r]++;
                    h[3, 128 + g]++;
                    h[3, 256 + b]++;
                }
            }
            int hmax = h[3, 0];
            for (i = 1; i < 384; i++)
            {
                if (h[3, i] > hmax)
                    hmax = h[3, i];
            }
            for (i = 0; i < 384; i++)
                h[3, i] = 120 * h[3, i] / hmax;
            for (int i = 0; i < 384; i++)
            {
                dataGraph.Add(i, h[3, i]);
            }
            LineItem myCurve = myPane5.AddCurve("data", dataGraph, Color.Black, SymbolType.None);
            myCurve.Line.Fill = new Fill(Color.Black, Color.Black, 45f);
            myPane5.XAxis.Scale.Max = 386;
            zedGraphControl5.AxisChange();
            myPane5.AxisChange();
            zedGraphControl5.Refresh();
        }
       
    }
}
