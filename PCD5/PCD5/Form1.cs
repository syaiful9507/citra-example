﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace PCD5
{
    public partial class Form1 : Form
    {

        Bitmap objBitmap1;
        Bitmap objBitmap2;
        int[,] mat = new int[1000, 1000];
        public Form1()
        {
            InitializeComponent();
        }

        private void btnload_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                objBitmap1 = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Image = objBitmap1;
            } 

            for (int x = 0; x < objBitmap1.Width; x++)
                for (int y = 0; y < objBitmap1.Height; y++)
                {
                    Color w = objBitmap1.GetPixel(x, y);
                    int wr = w.R;
                    int wg = w.G;
                    int wb = w.B;
                    int xg = (int)((wr + wg + wb) / 3);
                    if (xg > 127)
                        xg = 255;
                    else
                        xg = 0;
                    mat[x, y] = (int)xg / 255;
                    Color new_c = Color.FromArgb(xg, xg, xg);
                    objBitmap1.SetPixel(x, y, new_c);
                }
        }

        private void btnproyeksi_Click(object sender, EventArgs e)
        {
            int[] konx = new int[1000]; 
            int[] ii = new int[1000];
            var series1 = new Series("Proyeksi-Horizontal");

            for (int i = 0; i < objBitmap1.Width; i++)
            {
                konx[i] = 0;
                for (int j = 0; j < objBitmap1.Height; j++)
                {
                    if (mat[i, j] == 0) mat[i, j] = 1; else mat[i, j] = 0;
                    konx[i] = konx[i] + mat[i, j];
                    ii[i] = i + 1;
                }
                chart1.Series["Series1"].Points.AddXY(i, konx[i]);
            }
        }

        private void btnpvertikal_Click(object sender, EventArgs e)
        {
            int[] kony = new int[1000];
            int[] ii = new int[1000];
            var series1 = new Series("Proyeksi-Vertikal");

            for (int i = 0; i < objBitmap1.Height; i++)
            {
                ii[i] = 0;
                for (int j = 0; j < objBitmap1.Width; j++)
                {
                    if (mat[j, i] == 1) mat[j, i] = 0; else mat[j, i] = 1;
                    ii[i] = ii[i] + mat[j, i];
                    kony[i] = i - 1;
                }
                chart2.Series["Series1"].Points.AddXY(kony, ii[i]);
            }
        }     
    }
}
