﻿namespace Modul10Zedgraph
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_load = new System.Windows.Forms.Button();
            this.btn_hr = new System.Windows.Forms.Button();
            this.btn_hg = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_load
            // 
            this.btn_load.Location = new System.Drawing.Point(45, 12);
            this.btn_load.Name = "btn_load";
            this.btn_load.Size = new System.Drawing.Size(100, 42);
            this.btn_load.TabIndex = 0;
            this.btn_load.Text = "LOAD";
            this.btn_load.UseVisualStyleBackColor = true;
            // 
            // btn_hr
            // 
            this.btn_hr.Location = new System.Drawing.Point(151, 12);
            this.btn_hr.Name = "btn_hr";
            this.btn_hr.Size = new System.Drawing.Size(93, 42);
            this.btn_hr.TabIndex = 1;
            this.btn_hr.Text = "HR";
            this.btn_hr.UseVisualStyleBackColor = true;
            // 
            // btn_hg
            // 
            this.btn_hg.Location = new System.Drawing.Point(250, 12);
            this.btn_hg.Name = "btn_hg";
            this.btn_hg.Size = new System.Drawing.Size(95, 42);
            this.btn_hg.TabIndex = 2;
            this.btn_hg.Text = "HG";
            this.btn_hg.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 406);
            this.Controls.Add(this.btn_hg);
            this.Controls.Add(this.btn_hr);
            this.Controls.Add(this.btn_load);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_load;
        private System.Windows.Forms.Button btn_hr;
        private System.Windows.Forms.Button btn_hg;
    }
}

