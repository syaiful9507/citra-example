﻿namespace Modul4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btn_load = new System.Windows.Forms.Button();
            this.btn_grayscale = new System.Windows.Forms.Button();
            this.btn_hitamputih = new System.Windows.Forms.Button();
            this.btnkuantisasi = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(22, 69);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(282, 232);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(357, 69);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(320, 232);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btn_load
            // 
            this.btn_load.Location = new System.Drawing.Point(22, 26);
            this.btn_load.Name = "btn_load";
            this.btn_load.Size = new System.Drawing.Size(113, 33);
            this.btn_load.TabIndex = 2;
            this.btn_load.Text = "LOAD";
            this.btn_load.UseVisualStyleBackColor = true;
            this.btn_load.Click += new System.EventHandler(this.btn_load_Click);
            // 
            // btn_grayscale
            // 
            this.btn_grayscale.Location = new System.Drawing.Point(158, 26);
            this.btn_grayscale.Name = "btn_grayscale";
            this.btn_grayscale.Size = new System.Drawing.Size(104, 33);
            this.btn_grayscale.TabIndex = 3;
            this.btn_grayscale.Text = "GRAYSCALE";
            this.btn_grayscale.UseVisualStyleBackColor = true;
            this.btn_grayscale.Click += new System.EventHandler(this.btn_grayscale_Click);
            // 
            // btn_hitamputih
            // 
            this.btn_hitamputih.Location = new System.Drawing.Point(281, 26);
            this.btn_hitamputih.Name = "btn_hitamputih";
            this.btn_hitamputih.Size = new System.Drawing.Size(105, 33);
            this.btn_hitamputih.TabIndex = 4;
            this.btn_hitamputih.Text = "HITAM PUTIH";
            this.btn_hitamputih.UseVisualStyleBackColor = true;
            this.btn_hitamputih.Click += new System.EventHandler(this.btn_hitamputih_Click);
            // 
            // btnkuantisasi
            // 
            this.btnkuantisasi.Location = new System.Drawing.Point(412, 26);
            this.btnkuantisasi.Name = "btnkuantisasi";
            this.btnkuantisasi.Size = new System.Drawing.Size(102, 33);
            this.btnkuantisasi.TabIndex = 5;
            this.btnkuantisasi.Text = "KUANTISASI 16";
            this.btnkuantisasi.UseVisualStyleBackColor = true;
            this.btnkuantisasi.Click += new System.EventHandler(this.btnkuantisasi_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 331);
            this.Controls.Add(this.btnkuantisasi);
            this.Controls.Add(this.btn_hitamputih);
            this.Controls.Add(this.btn_grayscale);
            this.Controls.Add(this.btn_load);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn_load;
        private System.Windows.Forms.Button btn_grayscale;
        private System.Windows.Forms.Button btn_hitamputih;
        private System.Windows.Forms.Button btnkuantisasi;
    }
}

